//
//  ViewController.swift
//  ATPhoneNumber
//
//  Created by Dejan on 07/01/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

class ViewController: UIViewController {
    
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    let phoneUtil = NBPhoneNumberUtil()
    let formatter = NBAsYouTypeFormatter(regionCode: "US")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Parse
        let result = try? phoneUtil.parse("+12555554321", defaultRegion: "US")
        let formattedResult = try? phoneUtil.format(result, numberFormat: .INTERNATIONAL)
        print("formatted: \(formattedResult!)")
        
        // Example number
        let example = try? phoneUtil.getExampleNumber("US")
        print("example number: \((try? phoneUtil.format(example, numberFormat: .INTERNATIONAL))!)")
        print("is valid number: \(phoneUtil.isValidNumber(example))")
        print("number type: \(phoneUtil.getNumberType(example))")
        
        // Exctract phone number
        let extractedNumberString = try? phoneUtil.extractPossibleNumber("Well, the way they make shows is, they make one show. That show's called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they're going to make more shows. And let me just insert a random number here: +12555552145. Some pilots get picked and become television programs. Some don't, become nothing. She starred in one of the ones that became nothing.")
        let normalized = phoneUtil.normalizeDigitsOnly(extractedNumberString!)
        let extracted = try? phoneUtil.parse(normalized!, defaultRegion: "US")
        let formattedExtracted = try? phoneUtil.format(extracted, numberFormat: .INTERNATIONAL)
        print("Extracted: \(formattedExtracted!)")
        
        // Parse alpha
        let alphaParseString = phoneUtil.normalize("+1 201-555-FILM")
        let alphaParse = try? phoneUtil.parse(alphaParseString!, defaultRegion: "US")
        let alphaFormatted = try? phoneUtil.format(alphaParse, numberFormat: .INTERNATIONAL)
        print("Alpha parse: \(alphaFormatted!)")
    }
    
    @IBAction func clear() {
        self.numberLabel.text = "Start typing..."
        self.textField.text = nil
        self.formatter?.clear()
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        self.numberLabel.text = formatter?.inputDigit(string)
        return true
    }
}
